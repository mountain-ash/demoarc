﻿module Model

type Creds = string * string
type Route = string

type Config =
    | DbCfg of connection: string
    | ServACfg of Creds * initValue: int
    | ServBCfg of Creds
    | Zero

type Id =
    | Db1
    | Db2
    | ServA
    | ServB
    | Fibo

type Infrastructure<'T> =
    | SingleService of 'T
    | Service of 'T * Infrastructure<'T>
    | Uses of Infrastructure<'T>
    | Many of Infrastructure<'T> list

module Infrastructure =
    let rec map f =
        function
        | SingleService x -> f x |> SingleService
        | Service(x, i) -> Service(f x, map f i)
        | Uses x -> Uses(map f x)
        | Many xsS -> Many(xsS |> List.map (map f))

    let rec zip3 x y z =
        match x, y, z with
        | SingleService x, SingleService y, SingleService z -> SingleService(x, y, z)
        | Service(x, xi), Service(y, yi), Service(z, zi) -> Service((x, y, z), zip3 xi yi zi)
        | Uses x, Uses y, Uses z -> Uses(zip3 x y z)
        | Many x, Many y, Many z ->
            List.zip3 x y z
            |> List.map (fun (x, y, z) -> zip3 x y z)
            |> Many
        | _ -> invalidOp ""
        
    let rec toList x =
        let rec aux acc x =
            match x with 
            | SingleService x -> x::acc
            | Service(x, i) -> x::(aux acc i)
            | Uses x -> aux acc x 
            | Many xsS -> (xsS |> List.map (aux acc) |> List.collect id)
        aux [] x