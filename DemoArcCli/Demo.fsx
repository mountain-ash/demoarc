﻿﻿
(* IMPORTANT *)
(* before you start - run the command "dotnet run" in the current folder. 
    The command must run a program, asp.net server and listen to http://localhost:5000 *)

(* then send the content of this script into FSI line by line *)

#I @"tmp"
#r @"FSharp.Data.dll"
open FSharp.Data   


#load "Model.fs"
(*
That's how the solution can describe itself in 3 main parts.
Config - describes all possible configs in the system for objects, can be used for configuring similar components
Id - this structure defines objects/components in the solution
Infrastructure - defines hierarchy and relations between components also it serves as a DSL for moving services around
module Infrastructure ... provides standard functions for manupilating with the type Infrastructure
*)

(*
This module defines and populates objects accoding to the model
*)
#load "Configuration.fs"

(* This is sample configuration: *)
Configuration.initEnv()
(* 
DBs - depends on nothing;
service A uses 1 DB; 
service B is dependent on multiple other services;
service Fibo is recursive service which calls himself; 
*)

(*
it basicly provides such kind of mappings:
*)
open Model
type InitialObject = unit  -> Infrastructure<Config> 
type ConfiguredObject = string -> Infrastructure<Id>  -> Infrastructure<Config> 
type RoutedObject = Infrastructure<Id>  -> Infrastructure<Route> 

(* sample flow how to get a WebApp. Each step enriches the structure *)
(* 
The approach with objects came out with the fact that everything can be saved for further reuse, 
the ability to easily transfer the structure through any communication channels 
and edit it manually if necessary, say nothing about REPL approach.
Just a normal F# way:)
*)
let ids = Configuration.initEnv()
let cfg = Configuration.configureEnv "DEMO" ids
let rs = Configuration.routeEnv ids
let webAppModel = Infrastructure.zip3 ids cfg rs

(* Any possible handlers of requests *)
#load "ServiceImplementations.fs"

(* 
#load "WebAppBuilder.fs"
The place where all configs became actual web application. 
This module is closely related to the webapp runner which is Giraffe with big number of dependencies on Asp.Net.*)


(*
#load "Program.fsx"
That's a program. 
It acts as regular - takes system configurations, 
transforms them for the web host and runs the host with the given final objects.
*)

// check what I'm going to use
rs |> Infrastructure.toList |> List.distinct
(* let's execute several requests to chech whether the layout works as expected*)

// db1 returns a list from 1 to 4
Http.RequestString ("http://localhost:5000/db1?from=1&till=4")
(* the cmd window should show following log:
[service Db1] Fetching query { from = 1
    till = 4 } from Db1.DEMO.domain.com
*)

// the db2 returns the same result
Http.RequestString ("http://localhost:5000/db2?from=1&till=4")
(*
this is because they share the same implementation. But the configuration and location is different, so the cmd log shows the following:

[service Db2] Fetching query { from = 1
    till = 4 } from Db2.DEMO.domain.com

it was made on purpose, the Infrastructure can be agile until it makes sense and solves problems.
*)


Http.RequestString ("http://localhost:5000/serva?intValue=3")
(* you can see the flow: httpClient -> ServA -> Db1 -> ServA -> httpClient *)
(*
[Service ServA] Authorizing with creds ("username", "p@ssw0rd")
[service Db1] Fetching query { from = 100
  till = 103 } from Db1.DEMO.domain.com
*)

Http.RequestString ("http://localhost:5000/servb?intValue=3")
(* here you can see a little bit longer flow with with reuse of ServA: 
                            -> ServA -> Db1 |
    httpClient -> ServB -> |                  -> ServB -> httpClient 
                            -> Db2   ->     |
*)

(*
[Service ServB] Authorizing with creds ("username", "p@ssw0rd")
[Service ServA] Authorizing with creds ("username", "p@ssw0rd")
[service Db1] Fetching query { from = 100
  till = 103 } from Db1.DEMO.domain.com
[service Db2] Fetching query { from = 1000
  till = 1013 } from Db2.DEMO.domain.com
*)

(* and the interesting property of the designed solution,
there is a possibility to connect components recursively,
so the fibo service calls itself
*)

#time "on"
Http.RequestString ("http://localhost:5000/fibo?n=8")
(* thanks to the caching, timing is better for following requests and numbers *)
Http.RequestString ("http://localhost:5000/fibo?n=10")

