﻿open System
open System.Windows.Forms

open Giraffe

open Microsoft.Extensions.DependencyInjection
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Logging
open Model

let configureApp (app: IApplicationBuilder) =
    // Add Giraffe to the ASP.NET Core pipeline
    let ids = Configuration.initEnv()
    let cfg = Configuration.configureEnv "DEMO" ids
    let rs = Configuration.routeEnv ids
    let webAppModel = Infrastructure.zip3 ids cfg rs
    let webApp = WebAppBuilder.buildWebApp webAppModel
    app.UseGiraffe webApp

let configureServices (services: IServiceCollection) =
    // Add Giraffe dependencies
    services.AddGiraffe() |> ignore

let configureLogging (x: ILoggingBuilder) = x.AddConsole().AddDebug().SetMinimumLevel(LogLevel.Error) |> ignore

#if COMPILED
module BoilerPlateForForm =
    //[<System.STAThread>]
    do ()
    //do Application.Run(new Form(Text = "Hello"))
    WebHostBuilder().UseKestrel().Configure(Action<IApplicationBuilder> configureApp)
        .ConfigureServices(configureServices).ConfigureLogging(configureLogging).Build().Run()

#endif
