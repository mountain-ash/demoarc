﻿module Configuration

open Model
open Infrastructure

let initEnv(): Infrastructure<Id> =
    let fiboRec = SingleService(Fibo)
    let fibo = Service(Fibo, Uses fiboRec)

    let db1 = SingleService Db1
    let db2 = SingleService Db2
    let servA1 = Service(ServA, Uses db1)
    let servB1 = Service(ServB, Uses(Many [ db2; servA1 ]))
    Many [ db1; db2; servA1; servB1; fibo ]

let configureEnv envName infra: Infrastructure<Config> =
    let connStr id = sprintf "%A.%s.domain.com" id envName
    let sharedCreds = ("username", "p@ssw0rd") |> Creds
    infra
    |> map (function
        | Db1 -> connStr Db1 |> DbCfg
        | Db2 -> connStr Db2 |> DbCfg
        | ServA -> (sharedCreds, 100) |> ServACfg
        | ServB -> sharedCreds |> ServBCfg
        | Fibo -> Zero)

let routeEnv infra: Infrastructure<Route> =
    infra
    |> map (function
        | Db1 -> "/db1"
        | Db2 -> "/db2"
        | ServA -> "/serva"
        | ServB -> "/servb"
        | Fibo -> "/fibo")
 