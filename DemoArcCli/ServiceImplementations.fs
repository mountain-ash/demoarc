﻿module ServiceImplementations

open Model

type IReq =
    interface
    end

[<CLIMutable>]
type DbReq =
    { from: int
      till: int }
    interface IReq

[<CLIMutable>]
type ServReq =
    { intValue: int }
    interface IReq

[<CLIMutable>]
type FiboReq =
    { n: int }
    interface IReq

let dbReader id conn =
    fun (req: DbReq) ->
        printfn "[service %A] Fetching query %A from %s " id req conn
        [ req.from .. req.till ]

let servAReader id (creds, initValue) links =
    let dbReader =
        links
        |> List.find (fun (id, l) -> id = Db1)
        |> snd
    fun (req: ServReq) ->
        printfn "[Service %A] Authorizing with creds %A " id creds
        dbReader
            { from = initValue
              till = initValue + req.intValue }
        |> List.map ((*) 1 >> string)

let servBReader id creds links =

    let dbReader =
        links
        |> List.find (fun (id, l) -> id = Db2)
        |> snd

    let anotherService =
        links
        |> List.find (fun (id, l) -> id = ServA)
        |> snd

    fun (req: ServReq) ->
        printfn "[Service %A] Authorizing with creds %A " id creds
        let anotherServiceRes = req :> IReq |> anotherService

        let dbRes =
            { from = 1000
              till = 1010 + req.intValue }
            |> dbReader
        (anotherServiceRes, dbRes)

let fiboReader links =
    let fiboReader =
        links
        |> List.find (fun (id, l) -> id = Fibo)
        |> snd

    fun { n = n } ->
        let recurs n = { n = n } |> fiboReader
        if n < 2 then 1
        else recurs (n - 2) + recurs (n - 1)
 