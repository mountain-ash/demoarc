﻿module WebAppBuilder

open Model
open ServiceImplementations
open Giraffe
open FSharp.Data
open Newtonsoft.Json

let getHttpReader r (x: 'Tin): 'TOut =
    let url = "http://localhost:5000" + r
    JsonConvert.SerializeObject x
    |> fun x ->
        Http.RequestString
            (url, headers = [ HttpRequestHeaders.ContentType HttpContentTypes.Json ],
             body = HttpRequestBody.TextRequest x)
    |> JsonConvert.DeserializeObject<'TOut>

let rec buildLinks (infra: Infrastructure<Id * Config * string>): List<Id * (#IReq -> _)> =
    match infra with
    | SingleService(id, _, r)
    | SingleService(id, _, r)
    | Service((id, _, r), _)
    | Service((id, _, r), _) -> [ id, getHttpReader r ]
    | Many xs ->
        xs
        |> List.map buildLinks
        |> List.collect id
    | _ -> invalidArg "infra" (sprintf "%A" infra)

let bindModelToHandler h = bindModel None (h >> Successful.OK)

let buildWebApp (infra: Infrastructure<Id * Config * string>) =
    let rec aux ctx infra =
        match infra with
        | SingleService(Db1 as id, DbCfg conn, r)
        | SingleService(Db2 as id, DbCfg conn, r) ->
            [ route r >=> bindModelToHandler (dbReader id conn) ]
        | Many infras ->
            infras
            |> List.map (aux ctx)
            |> List.collect id
        | Service((ServA as id, ServACfg(creds, initValue), r), Uses links) ->
            let links = links |> buildLinks
            [ route r >=> bindModelToHandler (servAReader id (creds, initValue) links) ]
        | Service((ServB as id, ServBCfg creds, r), Uses links) ->
            let links = links |> buildLinks
            [ route r >=> bindModelToHandler (servBReader id creds links) ]
        | Service((Fibo, _, r), Uses links) ->
            let links = links |> buildLinks
            [ route r >=> bindModelToHandler (fiboReader links) ]
        | _ -> invalidOp "infra"

    infra
    |> aux []
    |> choose
